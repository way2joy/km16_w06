str_vector <- c("hello,", "me","?", "it", "looking", "is", "you", "for")
str_vector
factorial(8)

(start.time <- Sys.time())
output <- NULL
for (j in 1:100) {
  for (i in 1:100000) {
    dummy_str <- str_vector[sample(1:8,8,replace=F)]
    if(sum( dummy_str == str_vector[c(1,6,4,2,7,5,8,3)]) == 8) {
      print(j)
      print(i)
      output <- c(output, i)
      break
    }
  }
}
Sys.time() - start.time
mean(output)
